# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = User.create(first_name: "University", last_name: "Admin", date_of_birth: DateTime.parse("01/01/1991"), 
	gender: "Male", student_number: "000000", email: "clubbiz_uniadmin@unimelb.edu.au", password: "123456789", 
	password_confirmation: "123456789", phone_number: "0400000000")

sys = Webapp.create(university_admin: admin.id)


sande = User.create(first_name: "Sande", last_name: "Harsa", date_of_birth: DateTime.parse("01/01/1991"), 
	gender: "Male", student_number: "123456", email: "sharsa@uni.edu", password: "123123123", 
	password_confirmation: "123123123", phone_number: "0101010101")
johan = User.create(first_name: "Johan", last_name: "Albert", date_of_birth: DateTime.parse("10/10/1991"), 
	gender: "Male", student_number: "654321", email: "albertj@uni.edu", password: "123123123", 
	password_confirmation: "123123123", phone_number: "0101010101")
andy = User.create(first_name: "Andy", last_name: "Chen", date_of_birth: DateTime.parse("31/01/1993"), 
	gender: "Male", student_number: "567123", email: "anchen@uni.edu", password: "123123123", 
	password_confirmation: "123123123", phone_number: "0101010101")

df = DiscussionForum.create
club = Club.create(name: "Ajo-san", registration_number: "123456", website: "ajosan.com",
	club_email: "ajosan@gmail.com", description: "SMD Group", category: "Music",
	president_id: sande.id, discussion_forum_id: df.id, likes: 0)
Membership.create(is_admin: 1, user_id: sande.id, club_id: club.id)
Membership.create(is_admin: 0, user_id: johan.id, club_id: club.id)

df2 = DiscussionForum.create
club2 = Club.create(name: "JoA-san", registration_number: "654321", website: "joasan.com",
	club_email: "joasan@gmail.com", description: "Another SMD Group", category: "Music",
	president_id: andy.id, discussion_forum_id: df2.id, likes: 0)
Membership.create(is_admin: 1, user_id: andy.id, club_id: club2.id)
Membership.create(is_admin: 0, user_id: sande.id, club_id: club2.id)
Membership.create(is_admin: 1, user_id: johan.id, club_id: club2.id)

df3 = DiscussionForum.create
club3 = Club.create(name: "SanAJo", registration_number: "321456", website: "sanajo.com",
	club_email: "sanajo@gmail.com", description: "Yet another SMD Group", category: "Music",
	president_id: johan.id, discussion_forum_id: df3.id, likes: 0)
Membership.create(is_admin: 0, user_id: andy.id, club_id: club3.id)
Membership.create(is_admin: 0, user_id: sande.id, club_id: club3.id)
Membership.create(is_admin: 1, user_id: johan.id, club_id: club3.id)