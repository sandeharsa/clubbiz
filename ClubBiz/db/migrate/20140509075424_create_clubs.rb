class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :name
      t.string :registration_number
      t.string :website
      t.string :club_email
      t.text :description
      t.string :category
      t.integer :president_id
      t.integer :likes
      t.belongs_to :discussion_forum
      t.text :banner_image

      t.timestamps
    end
  end
end
