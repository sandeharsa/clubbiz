class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.belongs_to :user
      t.belongs_to :event
      t.integer :paid

      t.timestamps
    end
  end
end
