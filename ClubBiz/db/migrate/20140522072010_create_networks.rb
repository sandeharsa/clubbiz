class CreateNetworks < ActiveRecord::Migration
  def change
    create_table :networks do |t|
      t.string :name
      t.integer :created_by
      t.belongs_to :discussion_forum

      t.timestamps
    end
  end
end
