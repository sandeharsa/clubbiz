class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.datetime :event_datetime
      t.datetime :end_datetime
      t.string :location
      t.text :description
      t.float :ticket_price
      t.integer :tickets_total
      t.integer :current_tickets
      t.string :purchase_location
      t.date :sold_from
      t.integer :likes
      t.belongs_to :club
      t.belongs_to :discussion_forum
      t.text :banner_image
      t.text :other_image_1
      t.text :other_image_2
      t.text :other_image_3

      t.timestamps
    end
  end
end
