class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.integer :is_admin
      t.belongs_to :user
      t.belongs_to :club

      t.timestamps
    end
  end
end
