class CreateOfficeHolderPositions < ActiveRecord::Migration
  def change
    create_table :office_holder_positions do |t|
      t.string :position
      t.text :description
      t.belongs_to :user
      t.belongs_to :club

      t.timestamps
    end
  end
end
