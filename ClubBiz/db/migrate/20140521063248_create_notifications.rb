class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.text :message
      t.integer :source_id
      t.string :link

      t.timestamps
    end
  end
end
