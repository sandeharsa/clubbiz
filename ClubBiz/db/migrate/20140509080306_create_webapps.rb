class CreateWebapps < ActiveRecord::Migration
  def change
    create_table :webapps do |t|
      t.integer :university_admin

      t.timestamps
    end
  end
end
