class CreateClubLikes < ActiveRecord::Migration
  def change
    create_table :club_likes do |t|
      t.belongs_to :club
      t.belongs_to :user

      t.timestamps
    end
  end
end
