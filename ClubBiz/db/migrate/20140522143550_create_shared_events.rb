class CreateSharedEvents < ActiveRecord::Migration
  def change
    create_table :shared_events do |t|
      t.belongs_to :event
      t.belongs_to :network

      t.timestamps
    end
  end
end
