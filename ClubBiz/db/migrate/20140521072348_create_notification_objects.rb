class CreateNotificationObjects < ActiveRecord::Migration
  def change
    create_table :notification_objects do |t|
      t.belongs_to :user
      t.belongs_to :notification
      t.integer :read_status
      t.timestamps
    end
  end
end
