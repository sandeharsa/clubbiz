class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title
      t.integer :created_by
      t.integer :views
      t.belongs_to :discussion_forum

      t.timestamps
    end
  end
end
