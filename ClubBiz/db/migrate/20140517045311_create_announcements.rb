class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.string :title
      t.text :content
      t.integer :created_by
      t.belongs_to :club

      t.timestamps
    end
  end
end
