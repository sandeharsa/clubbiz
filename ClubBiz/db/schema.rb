# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140522143550) do

  create_table "announcements", force: true do |t|
    t.string   "title"
    t.text     "content"
    t.integer  "created_by"
    t.integer  "club_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "club_likes", force: true do |t|
    t.integer  "club_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "club_networks", force: true do |t|
    t.integer  "club_id"
    t.integer  "network_id"
    t.integer  "accepted"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clubs", force: true do |t|
    t.string   "name"
    t.string   "registration_number"
    t.string   "website"
    t.string   "club_email"
    t.text     "description"
    t.string   "category"
    t.integer  "president_id"
    t.integer  "likes"
    t.integer  "discussion_forum_id"
    t.text     "banner_image"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "discussion_forums", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "event_likes", force: true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "title"
    t.datetime "event_datetime"
    t.datetime "end_datetime"
    t.string   "location"
    t.text     "description"
    t.float    "ticket_price"
    t.integer  "tickets_total"
    t.integer  "current_tickets"
    t.string   "purchase_location"
    t.date     "sold_from"
    t.integer  "likes"
    t.integer  "club_id"
    t.integer  "discussion_forum_id"
    t.text     "banner_image"
    t.text     "other_image_1"
    t.text     "other_image_2"
    t.text     "other_image_3"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "memberships", force: true do |t|
    t.integer  "is_admin"
    t.integer  "user_id"
    t.integer  "club_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "networks", force: true do |t|
    t.string   "name"
    t.integer  "created_by"
    t.integer  "discussion_forum_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notification_objects", force: true do |t|
    t.integer  "user_id"
    t.integer  "notification_id"
    t.integer  "read_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: true do |t|
    t.text     "message"
    t.integer  "source_id"
    t.string   "link"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "office_holder_positions", force: true do |t|
    t.string   "position"
    t.text     "description"
    t.integer  "user_id"
    t.integer  "club_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.text     "content"
    t.integer  "topic_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shared_events", force: true do |t|
    t.integer  "event_id"
    t.integer  "network_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscriptions", force: true do |t|
    t.integer  "user_id"
    t.integer  "topic_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tickets", force: true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.integer  "paid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "topics", force: true do |t|
    t.string   "title"
    t.integer  "created_by"
    t.integer  "views"
    t.integer  "discussion_forum_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.date     "date_of_birth"
    t.string   "student_number"
    t.string   "phone_number"
    t.string   "gender"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "photo"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

  create_table "webapps", force: true do |t|
    t.integer  "university_admin"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
