ClubBiz::Application.routes.draw do
  get "user/index"
  get "user/show"
  get '/user_details' => 'user#show', as: :show_user

  get "webapps/search" => 'webapps#search'
  get "webapps/about_us" => 'webapps#about_us'

  get "notification_objects/read" => 'notification_objects#read'
  get "notification_objects/unread" => 'notification_objects#unread'

  get "clubs/add_member"
  get "clubs/remove_member"
  get "clubs/like"
  get "clubs/unlike"
  get "clubs/invite"
  get "clubs/uninvite"
  get "clubs/accept_invite"
  get "clubs/add_admin"
  get "clubs/remove_admin"
  get 'clubs/:id/admin' => 'clubs#admin', as: :club_admin

  get "topics/subscribe"
  get "topics/unsubscribe"

  get "announcements/system_announcements" => 'announcements#system_announcements', as: :sys_ann

  get 'events/reserve_tickets' => 'events#reserve_tickets'
  get 'events/reserve_ticket'
  get "events/like"
  get "events/unlike"
  get "events/share"
  get "events/unshare"

  delete '/tickets/:id(.:format)' => 'tickets#destroy', as: :delete_ticket

  get 'browse/clubs', to: 'clubs#browse'
  get 'browse/events', to: 'events#browse'

  get "networks/:id/invite_clubs" => 'networks#invite_clubs', as: :invite
  get "networks/:id/share_events" => 'networks#share_events', as: :share_event

  resources :networks

  resources :tickets

  resources :office_holder_positions

  resources :announcements

  resources :posts

  resources :topics

  resources :discussion_forums

  resources :events

  resources :notifications

  resources :webapps

  devise_for :users

  resources :clubs

  root to: "webapps#index"
  get "/newsfeed" => "announcements#news_feed"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
