# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ClubBiz::Application.config.secret_key_base = 'b53dafa7c7b9a450934b58ea5babd2df0efbf216e04cde64dd8b3265b74f6602a2a0d3d41576a2f92ca03235f689011ce8135f728e65464ea11ee3742b8a5987'
