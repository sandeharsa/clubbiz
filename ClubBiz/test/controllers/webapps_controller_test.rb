require 'test_helper'

class WebappsControllerTest < ActionController::TestCase
  setup do
    @webapp = webapps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:webapps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create webapp" do
    assert_difference('Webapp.count') do
      post :create, webapp: {  }
    end

    assert_redirected_to webapp_path(assigns(:webapp))
  end

  test "should show webapp" do
    get :show, id: @webapp
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @webapp
    assert_response :success
  end

  test "should update webapp" do
    patch :update, id: @webapp, webapp: {  }
    assert_redirected_to webapp_path(assigns(:webapp))
  end

  test "should destroy webapp" do
    assert_difference('Webapp.count', -1) do
      delete :destroy, id: @webapp
    end

    assert_redirected_to webapps_path
  end
end
