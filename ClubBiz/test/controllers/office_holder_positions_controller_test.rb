require 'test_helper'

class OfficeHolderPositionsControllerTest < ActionController::TestCase
  setup do
    @office_holder_position = office_holder_positions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:office_holder_positions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create office_holder_position" do
    assert_difference('OfficeHolderPosition.count') do
      post :create, office_holder_position: { description: @office_holder_position.description, position: @office_holder_position.position }
    end

    assert_redirected_to office_holder_position_path(assigns(:office_holder_position))
  end

  test "should show office_holder_position" do
    get :show, id: @office_holder_position
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @office_holder_position
    assert_response :success
  end

  test "should update office_holder_position" do
    patch :update, id: @office_holder_position, office_holder_position: { description: @office_holder_position.description, position: @office_holder_position.position }
    assert_redirected_to office_holder_position_path(assigns(:office_holder_position))
  end

  test "should destroy office_holder_position" do
    assert_difference('OfficeHolderPosition.count', -1) do
      delete :destroy, id: @office_holder_position
    end

    assert_redirected_to office_holder_positions_path
  end
end
