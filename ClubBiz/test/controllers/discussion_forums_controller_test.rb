require 'test_helper'

class DiscussionForumsControllerTest < ActionController::TestCase
  setup do
    @discussion_forum = discussion_forums(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:discussion_forums)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create discussion_forum" do
    assert_difference('DiscussionForum.count') do
      post :create, discussion_forum: {  }
    end

    assert_redirected_to discussion_forum_path(assigns(:discussion_forum))
  end

  test "should show discussion_forum" do
    get :show, id: @discussion_forum
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @discussion_forum
    assert_response :success
  end

  test "should update discussion_forum" do
    patch :update, id: @discussion_forum, discussion_forum: {  }
    assert_redirected_to discussion_forum_path(assigns(:discussion_forum))
  end

  test "should destroy discussion_forum" do
    assert_difference('DiscussionForum.count', -1) do
      delete :destroy, id: @discussion_forum
    end

    assert_redirected_to discussion_forums_path
  end
end
