class User < ActiveRecord::Base

  mount_uploader :photo, PhotoUploader

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :memberships
  has_many :clubs, through: :memberships
  has_many :posts
  has_many :office_holder_positions
  has_many :tickets
  has_many :club_likes
  has_many :subscriptions
  has_many :event_likes
  has_many :notifcation_objects


  validates :first_name, format: { with: /\A[A-Z]\w+\z/, 	
	  message: "First name is incorrectly formatted!" }

  validates :last_name, format: { with: /\A[A-Z]\w+\z/, 	
	  message: "is incorrectly formatted!" }

  validates :phone_number, format: { with: /\A\+?\d+\z/, 	
	  message: "is incorrectly formatted!" }

  def full_name
    "#{first_name} #{last_name}"
  end

  def print_user_link
  end

end
