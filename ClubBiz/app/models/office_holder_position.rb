class OfficeHolderPosition < ActiveRecord::Base
	belongs_to :user
	belongs_to :club

	validates_uniqueness_of :user_id, :scope => [:club_id], 
		message: "is already an office holder for this club."
end
