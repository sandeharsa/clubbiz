class DiscussionForum < ActiveRecord::Base
	has_many :topics
	has_one :club
	has_one :event
	has_one :network

	def belongs
		if Club.find_by(discussion_forum_id: id)
			return "club"
        elsif Event.find_by(discussion_forum_id: id)
        	return "event"
        else
        	return "network"
        end
    end

    def club(user_id)
    	if Club.find_by(discussion_forum_id: id)
			return Club.find_by(discussion_forum_id: id)
        elsif Event.find_by(discussion_forum_id: id)
        	event = Event.find_by(discussion_forum_id: id)
        	return Club.find_by(id: event.club_id)
        else
            network = Network.find_by(discussion_forum_id: id)
            ClubNetwork.where(network_id: network.id).each do |c|
                club = Club.find_by(id: c.club_id)
                if club.is_current_admin(user_id)
                   return club
                end
            end
        end
    end
end
