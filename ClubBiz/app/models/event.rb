class Event < ActiveRecord::Base

	mount_uploader :banner_image, BannerImageUploader
	mount_uploader :other_image_1, OtherImageUploader
	mount_uploader :other_image_2, OtherImageUploader
	mount_uploader :other_image_3, OtherImageUploader

	belongs_to :club
	belongs_to :discussion_forum
	has_many :tickets
	has_many :event_likes
	has_many :shared_events
end
