class Topic < ActiveRecord::Base
	belongs_to :discussion_forum
	has_many :posts
	has_many :subscriptions
end
