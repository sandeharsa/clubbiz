class Network < ActiveRecord::Base
	has_many :club_networks
	belongs_to :discussion_forum
	has_many :shared_events
end
