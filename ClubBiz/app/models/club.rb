class Club < ActiveRecord::Base

	mount_uploader :banner_image, BannerImageUploader

	has_many :memberships
	has_many :users, through: :memberships
	has_many :events
	belongs_to :discussion_forum
	has_many :announcements
	has_many :office_holder_positions
	has_many :club_likes
	has_many :club_networks

	def is_current_admin(user_id)
		if Membership.exists?(user_id: user_id, club_id: id)
			if Membership.find_by(user_id: user_id, club_id: id).is_admin == 1
				return true
			else
				return false
			end
		else
			return false
		end
	end
end
