class SystemMailer < ActionMailer::Base
  default from: "example@student.unimelb.edu.au"

  def welcome_user_email(user)
  	@user = user
  	@url = "http://localhost:3000/users/sign_in"
  	mail(to: @user.email, subject: "Welcome to Club-Biz")
  end

  def test_email
  	email = "andychen@student.unimelb.edu.au"
  	mail(to: email, subject: "Testing Mailer")
  end
end
