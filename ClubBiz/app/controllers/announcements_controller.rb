class AnnouncementsController < ApplicationController
  before_action :set_announcement, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /announcements
  # GET /announcements.json
  def index
    @announcements = Announcement.all
    @announcement = Announcement.new
    @cid = params[:club_id]

  end

  # GET /announcements/1
  # GET /announcements/1.json
  def show
  end

  # GET /announcements/new
  def new
    @announcement = Announcement.new
  end

  # GET /announcements/1/edit
  def edit
  end

  def system_announcements
    @announcement = Announcement.new
    @announcements = Announcement.where(created_by: Webapp.first.university_admin)
  end

  # POST /announcements
  # POST /announcements.json
  def create
    @announcement = Announcement.new(announcement_params)

    @announcement.created_by = current_user.id

    respond_to do |format|
      if @announcement.save
        format.html { redirect_to announcements_path(:club_id => @announcement.club_id), notice: 'Announcement was successfully created.' }
        format.json { render action: 'show', status: :created, location: @announcement }
      else
        format.html { render action: 'new' }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end

    if current_user.id == Webapp.first.university_admin
      cid = @announcement.club_id
      Club.all.each do |club|
        if club.id != cid
          @announcement = Announcement.new(announcement_params)
          @announcement.created_by = current_user.id
          @announcement.club_id = club.id
          @announcement.save
        end
      end
    end

  end

  # PATCH/PUT /announcements/1
  # PATCH/PUT /announcements/1.json
  def update
    respond_to do |format|
      if @announcement.update(announcement_params)
        format.html { redirect_to announcements_path(:club_id => @announcement.club_id), notice: 'Announcement was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /announcements/1
  # DELETE /announcements/1.json
  def destroy
    cid = @announcement.club_id
    @announcement.destroy
    respond_to do |format|
      format.html { redirect_to announcements_path(:club_id => cid) }
      format.json { head :no_content }
    end
  end

  def news_feed
    @announcements = Array.new
    Membership.where(user_id: current_user.id).each do |ms|
      Announcement.where(club_id: ms.club_id).each do |ann|
        if ann.created_by != Webapp.first.university_admin
          @announcements << ann
        end
      end
    end

    Announcement.where(created_by: Webapp.first.university_admin).uniq.pluck(:content).each do |content|
      ann = Announcement.find_by(created_by: Webapp.first.university_admin, content: content)
      @announcements << ann
    end

    @sorted = @announcements.sort_by &:created_at
    @sorted = @sorted.reverse
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_announcement
      @announcement = Announcement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def announcement_params
      params.require(:announcement).permit(:title, :content, :created_by, :club_id)
    end
end
