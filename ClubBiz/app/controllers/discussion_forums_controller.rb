class DiscussionForumsController < ApplicationController
  before_action :set_discussion_forum, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /discussion_forums
  # GET /discussion_forums.json
  def index
    @discussion_forums = DiscussionForum.all
  end

  # GET /discussion_forums/1
  # GET /discussion_forums/1.json
  def show
    @topics = Topic.all
    @club = Club.find_by(id: params[:club_id])
  end

  # GET /discussion_forums/new
  def new
    @discussion_forum = DiscussionForum.new
  end

  # GET /discussion_forums/1/edit
  def edit
  end

  # POST /discussion_forums
  # POST /discussion_forums.json
  def create
    @discussion_forum = DiscussionForum.new(discussion_forum_params)

    respond_to do |format|
      if @discussion_forum.save
        format.html { redirect_to @discussion_forum, notice: 'Discussion forum was successfully created.' }
        format.json { render action: 'show', status: :created, location: @discussion_forum }
      else
        format.html { render action: 'new' }
        format.json { render json: @discussion_forum.errors, status: :unprocessable_entity }
      end
    end
    return @discussion_forum.id
  end

  # PATCH/PUT /discussion_forums/1
  # PATCH/PUT /discussion_forums/1.json
  def update
    respond_to do |format|
      if @discussion_forum.update(discussion_forum_params)
        format.html { redirect_to @discussion_forum, notice: 'Discussion forum was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @discussion_forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /discussion_forums/1
  # DELETE /discussion_forums/1.json
  def destroy
    while Topic.exists?(discussion_forum_id: @discussion_forum.id) do
      Topic.find_by(discussion_forum_id: @discussion_forum.id).destroy
    end
    
    @discussion_forum.destroy
    respond_to do |format|
      format.html { redirect_to discussion_forums_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_discussion_forum
      @discussion_forum = DiscussionForum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def discussion_forum_params
      params[:discussion_forum]
    end
end
