class NetworksController < ApplicationController
  before_action :set_network, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /networks
  # GET /networks.json
  def index
    @networks = Network.all
  end

  # GET /networks/1
  # GET /networks/1.json
  def show
    @club_networks = ClubNetwork.where(network_id: @network.id)
    if SharedEvent.exists?(network_id: @network.id)
      @shared_events = SharedEvent.where(network_id: @network.id)
    else
      @shared_events = nil
    end
    @topics = Topic.all
  end

  # GET /networks/new
  def new
    @network = Network.new
  end

  # GET /networks/1/edit
  def edit
  end

  def invite_clubs
    @clubs = Club.all
  end

  def share_events
    @network = Network.find_by(id: params[:id])
    @club = Club.find_by(id: params[:club_id])
    @events = Array.new
    if @club.is_current_admin(current_user.id)
        Event.where(club_id: @club.id).each do |e|
          if !SharedEvent.exists?(event_id: e.id, network_id: @network.id)
            @events << e
          end
        end
      
    end
    @events = @events.sort_by &:event_datetime
  end

  # POST /networks
  # POST /networks.json
  def create
    @network = Network.new(network_params)

    df = DiscussionForum.create
    @network.discussion_forum_id = df.id

    respond_to do |format|
      if @network.save
        ClubNetwork.create(:club_id => @network.created_by, :network_id => @network.id, :accepted => 1)
        format.html { redirect_to network_url(:id => @network.id, :club_id => @network.created_by), notice: 'Network was successfully created.' }
        format.json { render action: 'show', status: :created, location: @network }
      else
        format.html { render action: 'new' }
        format.json { render json: @network.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /networks/1
  # PATCH/PUT /networks/1.json
  def update
    respond_to do |format|
      if @network.update(network_params)
        format.html { redirect_to network_url(:id => @network.id, :club_id => @network.created_by), notice: 'Network was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @network.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /networks/1
  # DELETE /networks/1.json
  def destroy
    while ClubNetwork.exists?(network_id: @network.id) do
      ClubNetwork.find_by(network_id: @network.id).destroy
    end

    cid = @network.created_by

    @network.destroy
    respond_to do |format|
      format.html { redirect_to club_admin_url(:id => cid, :tab => "network") }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_network
        @network = Network.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def network_params
      params.require(:network).permit(:name, :created_by)
    end
end
