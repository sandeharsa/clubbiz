class NotificationObjectsController < ApplicationController
  before_action :set_notification, only: [:show, :edit, :update, :destroy]
  helper_method :dismiss

  # Mark a notification as read
  def read
    NotificationObject.find(params[:id]).update_attribute(:read_status, 1)
    redirect_to :back
  end

  # Mark a notification as unread
  def unread
    NotificationObject.find(params[:id]).update_attribute(:read_status, 0)
    redirect_to :back
  end


end
