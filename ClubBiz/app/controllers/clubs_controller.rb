class ClubsController < ApplicationController
  before_action :set_club, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  helper_method :add_member, :add_admin, :remove_admin, :like, :unlike, :invite, :uninvite, :accept_invite, :remove_member

  # GET /clubs
  # GET /clubs.json
  def index
    @memberships = Membership.where(user_id: current_user.id)
    @admin_clubs = Array.new
    @member_clubs = Array.new
    @memberships.each do |ms|
      club = Club.find_by(id: ms.club_id)
      if ms.is_admin == 1
        @admin_clubs << club
      else
        @member_clubs << club
      end
    end
    @admin_clubs = @admin_clubs.sort_by &:name
    @member_clubs = @member_clubs.sort_by &:name
  end

  # GET /clubs/1
  # GET /clubs/1.json
  def show
  end

  def browse
    @clubs = Club.all
  end

  # GET /clubs/new
  def new
    @club = Club.new
  end

  # GET /clubs/1/edit
  def edit
  end

  # GET /clubs/1/admin
  def admin
    @club = Club.find_by(id: params[:id])
    @memberships = Membership.where(club_id: @club.id)
    @club_networks = ClubNetwork.where(club_id: @club.id)
    @announcements = Announcement.where(created_by: Webapp.first.university_admin)

    if Membership.exists?(user_id: current_user.id, club_id: @club.id) && 
      Membership.find_by(user_id: current_user.id, club_id: @club.id).is_admin == 1 
      @is_admin = true
    else
      @is_admin = false
    end
  end

  # POST /clubs
  # POST /clubs.json
  def create
    @club = Club.new(club_params)
    @club.president_id = current_user.id
    @club.likes = 0

    df = DiscussionForum.create
    @club.discussion_forum_id = df.id
    respond_to do |format|
      if @club.save
        format.html { redirect_to @club, notice: 'Club was successfully created.' }
        format.json { render action: 'show', status: :created, location: @club }
      else
        format.html { render action: 'new' }
        format.json { render json: @club.errors, status: :unprocessable_entity }
      end
    end

    add_membership(@club.president_id,@club.id,1)
    add_president(@club.president_id,@club.id)

    # notify all clubs
    title = "Club-Biz has a new club!"
    message = "#{@club.name} has just registered with Club-Biz!"
    Club.all.each do |c|
      if c.id != @club.id
        @announcement = Announcement.create(:title => title, :content => message,
          :created_by => Webapp.first.university_admin, :club_id => c.id)
      end
    end
    
    # Create a notification that announces the new club
    club_link = url_for("clubs/#{@club.id}")
    msg = "The club \"#{@club.name}\" has been created!"
    notification = Notification.create(:message=>msg, :source_id=>@club.president_id, :link=>club_link)

    # Send the notification to all users in the webapp
    User.all.each do |user|
      NotificationObject.create(:user_id=>user.id, :notification_id=>notification.id, :read_status=>0)
    end
  end

  # PATCH/PUT /clubs/1
  # PATCH/PUT /clubs/1.json
  def update
    respond_to do |format|
      if @club.update(club_params)
        format.html { redirect_to @club, notice: 'Club was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @club.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clubs/1
  # DELETE /clubs/1.json
  def destroy
    # Delete all the memberships for this club
    while Membership.exists?(club_id: @club.id) do
      Membership.find_by(club_id: @club.id).destroy
    end

    while ClubLike.exists?(club_id: @club.id) do
      ClubLike.find_by(club_id: @club.id).destroy
    end
    
    while OfficeHolderPosition.exists?(club_id: @club.id) do
      OfficeHolderPosition.find_by(club_id: @club.id).destroy
    end

    while Announcement.exists?(club_id: @club.id) do
      Announcement.find_by(club_id: @club.id).destroy
    end

    while ClubNetwork.exists?(club_id: @club.id) do
      ClubNetwork.find_by(club_id: @club.id).destroy
    end

    Event.where(club_id: @club.id).each do |e|
      e.destroy
    end

    while Network.exists?(created_by: @club.id) do
      Network.find_by(created_by: @club.id).destroy
    end



    DiscussionForum.find_by(id: @club.discussion_forum_id).destroy

    @club.destroy
    respond_to do |format|
      format.html { redirect_to clubs_url }
      format.json { head :no_content }
    end
  end

  def add_membership(user_id, club_id, is_admin)
    Membership.create(:user_id=>user_id,:is_admin=>is_admin,:club_id=>club_id)
  end

  # Adding 
  def add_member
    if Membership.find_by(user_id: params[:user_id], club_id: params[:club_id])
      redirect_to :back, notice: "You are already a member of the club."
    else  
      add_membership(params[:user_id], params[:club_id], 0)
      redirect_to :back, notice: "Welcome, You are now part of the club!"
    end
    #add_membership(params[user_id], params[club_id], 0)
  end

  def remove_member
    Membership.find_by(id: params[:ms_id]).destroy
    redirect_to :back
  end

  def like
    ClubLike.create(:user_id => current_user.id, :club_id => params[:club_id])
    club = Club.find_by(id: params[:club_id])
    club.likes += 1
    club.save
    redirect_to :back
  end

  def unlike
    ClubLike.find_by(user_id: current_user.id, club_id: params[:club_id]).destroy
    club = Club.find_by(id: params[:club_id])
    club.likes -= 1
    if club.likes < 0
      club.likes = 0
    end
    club.save
    redirect_to :back
  end

  def invite
    ClubNetwork.create(:club_id => params[:club_id], :network_id => params[:network_id], :accepted => 0)
    redirect_to :back
  end

  def uninvite
    ClubNetwork.find_by(club_id: params[:club_id], network_id: params[:network_id]).destroy
    redirect_to :back
  end

  def accept_invite
    cn = ClubNetwork.find_by(club_id: params[:club_id], network_id: params[:network_id])
    cn.accepted = 1
    cn.save
    redirect_to :back
  end

  def add_president(user_id, club_id)
    @president_position = OfficeHolderPosition.new
    @president_position.position = "President"
    @president_position.description = "Charged with providing leadership and direction to the Committee, the President is responsible for ensuring that the Committee fulfils its responsibilities for the governance and success of the club."
    @president_position.user_id = user_id
    @president_position.club_id = club_id
    @president_position.save
  end

  def add_admin
    ms = Membership.find_by(id: params[:ms_id])
    ms.is_admin = 1
    ms.save
    redirect_to club_admin_url(:id => ms.club_id)
  end

  def remove_admin
    ms = Membership.find_by(id: params[:ms_id])
    ms.is_admin = 0
    ms.save
    redirect_to club_admin_url(:id => ms.club_id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_club
      @club = Club.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def club_params
      params.require(:club).permit(:name, :registration_number, :website, :description, :category, :club_email,
        :banner_image, :banner_image_cache)
    end
end
