class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  helper_method :subscribe, :unsubscribe

  # GET /topics
  # GET /topics.json
  def index
    @topics = Topic.all
  end

  # GET /topics/1
  # GET /topics/1.json
  def show
    @posts = Post.all
    @topic.views += 1
    @topic.save
    @show_options = false

    @df = DiscussionForum.find_by(id: @topic.discussion_forum_id)
    @club = Club.find_by(id: params[:club_id])
    if @club.is_current_admin(current_user.id)
        @show_options = true
    end
    if @df.belongs == "event"
      @event = Event.find_by(discussion_forum_id: @df.id)
    elsif @df.belongs == "network"
      @network = Network.find_by(discussion_forum_id: @df.id)
    end
  end

  # GET /topics/new
  def new
    @topic = Topic.new()
  end

  # GET /topics/1/edit
  def edit
  end

  # POST /topics
  # POST /topics.json
  def create
    @topic = Topic.new(topic_params)
    @topic.created_by = current_user.id
    @topic.views = 0

    df_id = @topic.discussion_forum_id
    club = DiscussionForum.find_by(id: df_id).club(current_user.id)

    respond_to do |format|
      if @topic.save
        format.html { redirect_to topic_path(:id => @topic.id, :club_id => club.id), notice: 'Topic was successfully created.' }
        format.json { render action: 'show', status: :created, location: @topic }
      else
        format.html { render action: 'new' }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topics/1
  # PATCH/PUT /topics/1.json
  def update
    df_id = @topic.discussion_forum_id
    club = DiscussionForum.find_by(id: df_id).club(current_user.id)

    respond_to do |format|
      if @topic.update(topic_params)
        format.html { redirect_to topic_path(:id => @topic.id, :club_id => club.id), notice: 'Topic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    # Delete all posts for this topic
    while Post.exists?(topic_id: @topic.id) do
      Post.find_by(topic_id: @topic.id).destroy
    end
    while Subscription.exists?(topic_id: @topic.id) do
      Subscription.find_by(topic_id: @topic.id).destroy
    end

    df_id = @topic.discussion_forum_id
    club = DiscussionForum.find_by(id: df_id).club(current_user.id)

    @topic.destroy
    respond_to do |format|
      format.html { redirect_to discussion_forum_path(:id => df_id, :club_id => club.id) }
      format.json { head :no_content }
    end
  end

  def subscribe
    Subscription.create(:user_id => current_user.id, :topic_id => params[:topic_id])
    redirect_to :back
  end

  def unsubscribe
    Subscription.find_by(user_id: current_user.id, :topic_id => params[:topic_id]).destroy
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic
      @topic = Topic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_params
      params.require(:topic).permit(:title, :created_by, :views, :discussion_forum_id)
    end
end
