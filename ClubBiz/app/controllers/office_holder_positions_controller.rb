class OfficeHolderPositionsController < ApplicationController
  before_action :set_office_holder_position, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /office_holder_positions
  # GET /office_holder_positions.json
  def index
    @office_holder_positions = OfficeHolderPosition.all
  end

  # GET /office_holder_positions/1
  # GET /office_holder_positions/1.json
  def show
  end

  # GET /office_holder_positions/new
  def new
    @office_holder_position = OfficeHolderPosition.new
  end

  # GET /office_holder_positions/1/edit
  def edit
  end

  # POST /office_holder_positions
  # POST /office_holder_positions.json
  def create
    @office_holder_position = OfficeHolderPosition.new(office_holder_position_params)

    respond_to do |format|
      if @office_holder_position.save
        format.html { redirect_to office_holder_positions_path(:club_id => @office_holder_position.club_id), notice: 'Office holder position was successfully created.' }
        format.json { render action: 'show', status: :created, location: @office_holder_position }
      else
        format.html { redirect_to new_office_holder_position_path(:club_id => @office_holder_position.club_id), alert: 'User is already an office holder of this club.' }
        format.json { render json: @office_holder_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /office_holder_positions/1
  # PATCH/PUT /office_holder_positions/1.json
  def update
    respond_to do |format|
      if @office_holder_position.update(office_holder_position_params)
        format.html { redirect_to office_holder_positions_path(:club_id => @office_holder_position.club_id), notice: 'Office holder position was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @office_holder_position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /office_holder_positions/1
  # DELETE /office_holder_positions/1.json
  def destroy
    cid = @office_holder_position.club_id
    @office_holder_position.destroy
    respond_to do |format|
      format.html { redirect_to office_holder_positions_path(:club_id => cid) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_office_holder_position
      @office_holder_position = OfficeHolderPosition.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def office_holder_position_params
      params.require(:office_holder_position).permit(:position, :description, :user_id, :club_id)
    end
end
