class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  helper_method :reserve_ticket, :like, :unlike, :share, :unshare

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
  end

  def browse
    @events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.current_tickets = @event.tickets_total
    @event.likes = 0

    df = DiscussionForum.create
    @event.discussion_forum_id = df.id

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render action: 'show', status: :created, location: @event }
      else
        format.html { render action: 'new' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end

    # Create a notification that announces the new event
    club_link = url_for("events/#{@event.id}")
    msg = "The event \"#{@event.title}\" has been created by \"#{Club.find(@event.club_id).name}\"!"
    notification = Notification.create(:message=>msg, :source_id=>current_user.id, :link=>club_link)

    # Send the notification to all users in the club that created the event
    Membership.where(club_id: @event.club_id).each do |ms|
      NotificationObject.create(:user_id=>ms.user_id, :notification_id=>notification.id, :read_status=>0)
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    while EventLike.exists?(event_id: @event.id) do
      EventLike.find_by(event_id: @event.id).destroy
    end

    while Ticket.exists?(event_id: @event.id) do
      Ticket.find_by(event_id: @event.id).destroy
    end

    DiscussionForum.find_by(id: @event.discussion_forum_id).destroy

    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  def reserve_ticket
    event_id = params[:event_id]
    quantity = params[:quantity].to_i
    @event = Event.find_by(id: event_id)

    repeat = Array.new(quantity,0)
    count = 0
    repeat.each do |a|
      if @event.current_tickets > 0
        Ticket.create(:event_id => event_id, :user_id => current_user.id, :paid => 0)
        @event.current_tickets = @event.current_tickets.to_i - 1
        @event.save
        count += 1
      else
        break
      end
    end

    if count < quantity
      redirect_to "/events/#{event_id}", notice: "Successfully reserved #{count} tickets."
    else
      redirect_to "/events/#{event_id}", notice: "Successfully reserved all tickets."
    end
  end

  def like
    EventLike.create(:user_id => current_user.id, :event_id => params[:event_id])
    event = Event.find_by(id: params[:event_id])
    event.likes += 1
    event.save
    redirect_to :back
  end

  def unlike
    EventLike.find_by(user_id: current_user.id, event_id: params[:event_id]).destroy
    event = Event.find_by(id: params[:event_id])
    event.likes -= 1
    if event.likes < 0
      event.likes = 0
    end
    event.save
    redirect_to :back
  end

  def share
    SharedEvent.create(:event_id => params[:event_id], :network_id => params[:network_id])
    redirect_to :back
  end

  def unshare
    SharedEvent.find_by(event_id: params[:event_id], network_id: params[:network_id]).destroy
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :event_datetime, :end_datetime, :location, 
        :description, :ticket_price, :tickets_total, :current_tickets, :purchase_location, 
        :sold_from, :club_id, :discussion_forum_id, :ticket_quantity, :quantity, :likes,
        :banner_image, :banner_image_cache, :other_image_1, :other_image_1_cache,
        :other_image_2, :other_image_2_cache, :other_image_3, :other_image_3_cache)
    end
end
