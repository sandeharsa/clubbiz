class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new()
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id

    df_id = Topic.find_by(id: @post.topic_id).discussion_forum_id
    club = DiscussionForum.find_by(id: df_id).club(current_user.id)

    respond_to do |format|
      if @post.save
        format.html { redirect_to topic_path(:id => @post.topic_id, :club_id => club.id), notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end

    # Create a notification that announces the new post
    topic_link = url_for("topics/#{@post.topic_id}?club_id=#{club.id}")
    msg = "#{User.find(@post.user_id).first_name} has posted on a topic that you are following."
    notification = Notification.create(:message=>msg, :source_id=>@post.user_id, :link=>topic_link)

    # Send the notification to all users in the webapp
    Subscription.where(topic_id: @post.topic_id).each do |sub|
      NotificationObject.create(:user_id=>sub.user_id, :notification_id=>notification.id, :read_status=>0)
    end

  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    df_id = Topic.find_by(id: @post.topic_id).discussion_forum_id
    club = DiscussionForum.find_by(id: df_id).club(current_user.id)
    
    @post.destroy
    respond_to do |format|
      format.html { redirect_to topic_path(:id => @post.topic_id, :club_id => club.id) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:content, :topic_id)
    end
end
