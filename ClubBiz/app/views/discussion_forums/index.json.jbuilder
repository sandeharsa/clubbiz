json.array!(@discussion_forums) do |discussion_forum|
  json.extract! discussion_forum, :id
  json.url discussion_forum_url(discussion_forum, format: :json)
end
