json.array!(@notifications) do |notification|
  json.extract! notification, :id, :message, :source_id, :link
  json.url notification_url(notification, format: :json)
end
