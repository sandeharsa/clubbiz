json.array!(@topics) do |topic|
  json.extract! topic, :id, :title, :created_by, :views
  json.url topic_url(topic, format: :json)
end
