json.array!(@events) do |event|
  json.extract! event, :id, :title, :event_datetime, :location, :description, :tickets_total, :current_tickets, :purchase_location
  json.url event_url(event, format: :json)
end
