json.array!(@announcements) do |announcement|
  json.extract! announcement, :id, :title, :content, :created_by
  json.url announcement_url(announcement, format: :json)
end
