json.array!(@office_holder_positions) do |office_holder_position|
  json.extract! office_holder_position, :id, :position, :description
  json.url office_holder_position_url(office_holder_position, format: :json)
end
