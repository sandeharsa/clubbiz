json.array!(@clubs) do |club|
  json.extract! club, :id, :name, :registration_number, :website, :description
  json.url club_url(club, format: :json)
end
