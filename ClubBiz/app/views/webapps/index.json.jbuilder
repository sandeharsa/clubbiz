json.array!(@webapps) do |webapp|
  json.extract! webapp, :id
  json.url webapp_url(webapp, format: :json)
end
